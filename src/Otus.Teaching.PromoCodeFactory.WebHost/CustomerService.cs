﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
	public class CustomerService
	{
		private readonly IRepository<Customer> _customerRepository;
		private readonly IRepository<Preference> _preferenceRepository;

		public CustomerService(IRepository<Customer> customerRepository,
			IRepository<Preference> preferenceRepository)
		{
			_customerRepository = customerRepository;
			_preferenceRepository = preferenceRepository;
		}

		public async Task<List<CustomerShortResponse>> GetCustomersAsync()
		{
			var customers = await _customerRepository.GetAllAsync();

			return customers.Select(x => new CustomerShortResponse()
			{
				Id = x.Id,
				Email = x.Email,
				FirstName = x.FirstName,
				LastName = x.LastName
			}).ToList();
		}

		
		public async Task<CustomerResponse> GetCustomerAsync(Guid id)
		{
			var customer = await _customerRepository.GetByIdAsync(id);
			if (customer == null) throw new ArgumentException(nameof(customer));
			return new CustomerResponse(customer);
		}

		
		public async Task<CustomerResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request)
		{
			//Получаем предпочтения из бд и сохраняем большой объект
			var preferences = await _preferenceRepository
				.GetRangeByIdsAsync(request.PreferenceIds);

			Customer customer = CustomerMapper.MapFromModel(request, preferences);

			await _customerRepository.AddAsync(customer);
			
			return new CustomerResponse(customer);
		}

		public async Task EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
		{
			var customer = await _customerRepository.GetByIdAsync(id);

			if (customer == null) throw new ArgumentException(nameof(customer));
				

			var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

			CustomerMapper.MapFromModel(request, preferences, customer);

			await _customerRepository.UpdateAsync(customer);			
		}

		
		public async Task DeleteCustomerAsync(Guid id)
		{
			var customer = await _customerRepository.GetByIdAsync(id);

			if (customer == null) throw new ArgumentException(nameof(customer));

			await _customerRepository.DeleteAsync(customer);			
		}


	}
}
