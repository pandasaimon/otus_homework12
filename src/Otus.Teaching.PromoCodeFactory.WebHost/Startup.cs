using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Gateways;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Integration;
using CustomerGrpcService;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using CustomerSignalRApi;


namespace Otus.Teaching.PromoCodeFactory.WebHost
{
	public class Startup
	{
		public IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			//AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

			services.AddControllers().AddMvcOptions(x =>
				x.SuppressAsyncSuffixInActionNames = false);
			services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
			services.AddScoped<INotificationGateway, NotificationGateway>();
			services.AddScoped<IDbInitializer, EfDbInitializer>();
			services.AddScoped<CustomerService>();
			services.AddDbContext<DataContext>(x =>
			{
				x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
				//x.UseNpgsql(Configuration.GetConnectionString("PromoCodeFactoryDb"),  o => o.SetPostgresVersion(12, 0));
				x.UseSnakeCaseNamingConvention();
				x.UseLazyLoadingProxies();
			});


			// Add services to the container.
			services.AddGrpc();
			services.AddGrpcReflection();



			services.AddOpenApiDocument(options =>
			{
				options.Title = "PromoCode Factory API Doc";
				options.Version = "1.0";
			});

			services.AddSignalR();

		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
		{
			// Configure the HTTP request pipeline.



			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.UseOpenApi();
			app.UseSwaggerUi(x =>
			{
				x.DocExpansion = "list";
			});

			app.UseHttpsRedirection();

			app.UseRouting();
			app.UseEndpoints(endpoints =>
			{
				//�������� Grpc
				endpoints.MapGrpcService<CustomerGrpcApi>();
				endpoints.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
				if (env.IsDevelopment())
				{
					endpoints.MapGrpcReflectionService();
				}

				endpoints.MapControllers();
				// �������� SignalR
				endpoints.MapHub<CustomerHub>("/hubs/customer");
			});


			dbInitializer.InitializeDb();
		}
	}
}