﻿using CustomerGrpcService;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
	public class CustomerGrpcApi : CustomerGrpcService.CustomerService.CustomerServiceBase
	{
		private readonly CustomerService _customerService;


		public CustomerGrpcApi(CustomerService customerService)
		{
			_customerService = customerService;
		}

		public override async Task<CustomersListResponseGrpc> GetAllCustomers(Empty request, ServerCallContext context)
		{
			var customers = await _customerService.GetCustomersAsync();
			var listResponse = new CustomersListResponseGrpc();

			foreach (var item in customers)
			{
				listResponse.Customers.Add(new CustomerShortResponseGrpc()
				{
					Id = item.Id.ToString(),
					Email = item.Email,
					FirstName = item.FirstName,
					LastName = item.LastName,
				});

			}
			return listResponse;
		}

		public override async Task<CustomerResponseGrpc> GetCustomer(GetRequestGrpc request, ServerCallContext context)
		{
			Guid customerId = ParseGuid(request.Id);
			try
			{
				var customer = await _customerService.GetCustomerAsync(customerId);
				if (customer == null) throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer with this id  is required."));

				CustomerResponseGrpc resp = new()
				{
					Id = customerId.ToString(),
					Email = customer.Email,
					FirstName = customer.FirstName,
					LastName = customer.LastName
				};
				foreach (var preference in customer.Preferences)
					resp.Preferences.Add(new PreferenceResponseGrpc()
					{
						Id = preference.Id.ToString(),
						Name = preference.Name,
					});

				return resp;
			}
			catch (ArgumentException)
			{
				throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer with this id  is required."));
			}
		}

		private static Guid ParseGuid(string id)
		{
			if (!Guid.TryParse(id, out Guid customerId)) throw new RpcException(new Status(StatusCode.InvalidArgument, "Invalid id format"));
			return customerId;
		}

		public override async Task<CustomerResponseGrpc> CreateCustomer(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
		{
			List<Guid> ids = new();
			
			foreach (var id in request.PreferenceIds)
			{
				ids.Add(ParseGuid(id));
			}
			CreateOrEditCustomerRequest requestDto = new CreateOrEditCustomerRequest()
			{
				PreferenceIds = ids,
				Email = request.Email,
				FirstName = request.FirstName,
				LastName = request.LastName
			};

			var customer = await _customerService.CreateCustomerAsync(requestDto);
			return CustomerMapper.MapFromModel(customer);
		}


		public override async Task<Empty> DeleteCustomer(DeleteRequestGrpc request, ServerCallContext context)
		{
			try
			{
				Guid customerId = ParseGuid(request.Id);
			await _customerService.DeleteCustomerAsync(customerId);			
			return new Empty();
			}
			catch (ArgumentException)
			{
				throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer with this id  is required."));
			}
		}

		public override async Task<Empty> EditCustomer(EditCustomerRequestGrpc request, ServerCallContext context)
		{
			try
			{
				Guid customerId = ParseGuid(request.Id);
				List<Guid> ids = new();
				foreach (var id in request.EditRequest.PreferenceIds)
				{
					ids.Add(ParseGuid(id));
				}
				CreateOrEditCustomerRequest requestDto = new CreateOrEditCustomerRequest()
				{
					PreferenceIds = ids,
					Email = request.EditRequest.Email,
					FirstName = request.EditRequest.FirstName,
					LastName = request.EditRequest.LastName
				};

				await _customerService.EditCustomersAsync(customerId, requestDto);
				return new Empty();
			}
			catch (ArgumentException)
			{
				throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer with this id  is required."));
			}

		}
	}
}
