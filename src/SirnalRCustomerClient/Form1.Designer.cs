﻿namespace SirnalRCustomerClient
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			buttonConnect = new Button();
			buttonGetAll = new Button();
			textBox1 = new TextBox();
			buttonGetOne = new Button();
			buttonCreate = new Button();
			buttonDelete = new Button();
			buttonEdit = new Button();
			SuspendLayout();
			// 
			// buttonConnect
			// 
			buttonConnect.Location = new Point(27, 34);
			buttonConnect.Name = "buttonConnect";
			buttonConnect.Size = new Size(75, 23);
			buttonConnect.TabIndex = 0;
			buttonConnect.Text = "Connect";
			buttonConnect.UseVisualStyleBackColor = true;
			buttonConnect.Click += buttonConnect_Click;
			// 
			// buttonGetAll
			// 
			buttonGetAll.Location = new Point(27, 97);
			buttonGetAll.Name = "buttonGetAll";
			buttonGetAll.Size = new Size(75, 23);
			buttonGetAll.TabIndex = 0;
			buttonGetAll.Text = "GetAll";
			buttonGetAll.UseVisualStyleBackColor = true;
			buttonGetAll.Click += buttonGetAll_Click;
			// 
			// textBox1
			// 
			textBox1.Location = new Point(174, 2);
			textBox1.Multiline = true;
			textBox1.Name = "textBox1";
			textBox1.Size = new Size(337, 479);
			textBox1.TabIndex = 1;
			// 
			// buttonGetOne
			// 
			buttonGetOne.Location = new Point(27, 143);
			buttonGetOne.Name = "buttonGetOne";
			buttonGetOne.Size = new Size(75, 23);
			buttonGetOne.TabIndex = 0;
			buttonGetOne.Text = "GetOne";
			buttonGetOne.UseVisualStyleBackColor = true;
			buttonGetOne.Click += buttonGetOne_Click;
			// 
			// buttonCreate
			// 
			buttonCreate.Location = new Point(27, 192);
			buttonCreate.Name = "buttonCreate";
			buttonCreate.Size = new Size(75, 23);
			buttonCreate.TabIndex = 0;
			buttonCreate.Text = "Create";
			buttonCreate.UseVisualStyleBackColor = true;
			buttonCreate.Click += buttonCreate_Click;
			// 
			// buttonDelete
			// 
			buttonDelete.Location = new Point(27, 233);
			buttonDelete.Name = "buttonDelete";
			buttonDelete.Size = new Size(75, 23);
			buttonDelete.TabIndex = 0;
			buttonDelete.Text = "Delete";
			buttonDelete.UseVisualStyleBackColor = true;
			buttonDelete.Click += buttonDelete_Click;
			// 
			// buttonEdit
			// 
			buttonEdit.Location = new Point(27, 291);
			buttonEdit.Name = "buttonEdit";
			buttonEdit.Size = new Size(75, 23);
			buttonEdit.TabIndex = 0;
			buttonEdit.Text = "Edit";
			buttonEdit.UseVisualStyleBackColor = true;
			buttonEdit.Click += buttonEdit_Click;
			// 
			// Form1
			// 
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(513, 483);
			Controls.Add(textBox1);
			Controls.Add(buttonEdit);
			Controls.Add(buttonDelete);
			Controls.Add(buttonCreate);
			Controls.Add(buttonGetOne);
			Controls.Add(buttonGetAll);
			Controls.Add(buttonConnect);
			Name = "Form1";
			Text = "Form1";
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private Button buttonConnect;
		private Button buttonGetAll;
		private TextBox textBox1;
		private Button buttonGetOne;
		private Button buttonCreate;
		private Button buttonDelete;
		private Button buttonEdit;
	}
}
